<?php

namespace App\Services;

use App\Dto\ProjectDto;
use App\Dto\TaskDto;
use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use App\Repositories\ProjectRepository;
use App\Repositories\TaskRepository;
use Exception;

/**
 * Class ProjectService
 *
 * @package App\Services
 */
class TaskService
{
    /**
     * @var ProjectRepository|null $projectRepository
     */
    private $projectRepository = null;
    /**
     * @var TaskRepository
     */
    private $taskRepository = null;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $projectRepository
     * @param TaskRepository $taskRepository
     */
    private function __construct(ProjectRepository $projectRepository, TaskRepository $taskRepository)
    {
        $this->projectRepository = $projectRepository;
        $this->taskRepository = $taskRepository;
    }

    /**
     * @param  array $params
     * @param  array $with
     * @param  bool  $asArray
     *
     * @return array
     */
    public function all(
        array $params,
        array $with = [],
        bool  $asArray = false
    ) {
        $projects = $this->taskRepository->whereArray($params)->with($with);

        $projects = $projects->get();

        if($asArray) {
            $projects = $projects->toArray();
        }

        return $projects->all();
    }

    /**
     * @param TaskDto $record
     * @param Project $project
     * @return Task
     */
    public function create(TaskDto $record, Project $project): Task
    {
        /**
         * @var Task $model
         */
        $model = $this->taskRepository->create(array_merge($record->all(), ['project_id' => $project->id]));

        return $model;
    }

    /**
     * @param TaskDto $record
     * @param Task $task
     * @return Task
     */
    public function update(TaskDto $record, Task $task): Task
    {
        /**
         * @var Task $model
         */
        $model = $this->taskRepository->updateByArray($task, $record->all(), true);

        return $model;
    }

    /**
     * @param  Task $model
     * @throws Exception
     */
    public function delete(Task $model): void
    {
        $this->taskRepository->delete($model);
    }
}
