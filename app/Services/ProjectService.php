<?php

namespace App\Services;

use App\Dto\ProjectDto;
use App\Models\Project;
use App\Models\User;
use App\Repositories\ProjectRepository;
use Exception;

/**
 * Class ProjectService
 *
 * @package App\Services
 */
class ProjectService
{
    /**
     * @var ProjectRepository|null $projectRepository
     */
    private $projectRepository = null;

    /**
     * ProjectService constructor.
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * @param  array $params
     * @param  array $with
     * @param  bool  $asArray
     *
     * @return array
     */
    public function all(
        array $params,
        array $with = [],
        bool  $asArray = false
    ) {
        $projects = $this->projectRepository->whereArray($params)->with($with);

        $projects = $projects->get();

        if($asArray) {
            $projects = $projects->toArray();
        }

        return $projects->all();
    }

    /**
     * @param ProjectDto $record
     * @param User $user
     * @return Project
     */
    public function create(ProjectDto $record, User $user): Project
    {
        /**
         * @var Project $model
         */
        $model = $this->projectRepository->create(array_merge($record->all(), ['owner_id' => $user->id]));

        return $model;
    }

    /**
     * @param ProjectDto $record
     * @param Project $project
     * @return Project
     */
    public function update(ProjectDto $record, Project $project): Project
    {
        /**
         * @var Project $model
         */
        $model = $this->projectRepository->updateByArray($project, $record->all(), true);

        return $model;
    }

    /**
     * @param  Project $model
     * @throws Exception
     */
    public function delete(Project $model): void
    {
        $this->projectRepository->delete($model);
    }
}
