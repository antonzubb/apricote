<?php


namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class ProjectDto extends DataTransferObject
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var string|null */
    public $notes;
}
