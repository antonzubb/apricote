<?php


namespace App\Dto;

use Spatie\DataTransferObject\DataTransferObject;

class TaskDto extends DataTransferObject
{
    /** @var boolean */
    public $completed;

    /** @var string */
    public $body;
}
