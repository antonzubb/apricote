<?php

namespace App\Http\Controllers;

use App\Dto\TaskDto;
use App\Http\Requests\CreateOrUpdateTaskProjectRequest;
use App\Models\Project;
use App\Models\Task;
use App\Services\TaskService;

class ProjectsTaskController extends Controller
{
    /**
     * @var TaskService
     */
    private $taskService;

    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    public function store(CreateOrUpdateTaskProjectRequest $request, Project $project)
    {
        $dto = new TaskDto($request->validated());
        $this->taskService->create($dto, $project);

        redirect($project->path());
    }

    public function update(CreateOrUpdateTaskProjectRequest $request, Project $project, Task $task)
    {
        $dto = new TaskDto($request->validated());
        $this->taskService->update($dto, $task);

        redirect($project->path());
    }
}
