<?php

namespace App\Http\Controllers;

use App\Dto\ProjectDto;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use App\Services\ProjectService;


class ProjectsController extends Controller
{
    /**
     * @var ProjectService
     */
    private $projectService;

    public function __construct(ProjectService $projectService)
    {
        $this->projectService = $projectService;
    }

    public function index()
    {
        $projects =  $this->projectService->all(['owner_id' => auth()->id()]);

        return view('projects.index', compact('projects'));
    }

    public function store(CreateProjectRequest $request)
    {

        $dto = new ProjectDto($request->validated());

        $this->projectService->create($dto, auth()->user());

        return redirect('/projects');
    }

    public function show(Project $project)
    {
        $this->authorize('update', $project);

        return view('projects.show', compact('project'));
    }

    public function create()
    {
        return view('projects.create');
    }

    public function update(UpdateProjectRequest $request, Project $project)
    {
        $dto = new ProjectDto($request->validated());

        $project = $this->projectService->update($dto, $project);

        return redirect($project->path());
    }

    public function edit(Project $project)
    {
        $this->authorize('update', $project);

        return view('projects.edit', compact($project));
    }

    public function delete(Project $project)
    {
        $this->authorize('update', $project);

        return redirect(route('/projects'));
    }
}
