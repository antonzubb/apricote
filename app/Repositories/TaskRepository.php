<?php

namespace App\Repositories;

use App\Models\Task;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class TaskRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = Task::class;
}
