<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as SupportCollection;
use IteratorAggregate;
use ReflectionClass;
use ReflectionException;

/**
 * Class AbstractRepository
 * @package App\Repository
 */
abstract class AbstractRepository
{
    public const MODEL_REPOSITORY = Model::class;

    /**
     * Find model by parameter
     *
     * @param  $param
     * @param  array $columns
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public function find($param, $columns = ["*"])
    {
        return $this->builder()->find($param, $columns);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->builder()->get();
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return $this->builder()->count();
    }

    /**
     * @param  string $field
     *
     * @return SupportCollection|null
     */
    public function pluck(string $field): ?SupportCollection
    {
        return $this->builder()->pluck($field);
    }

    /**
     * Find by field, first item from collection
     *
     * @param $field
     * @param $value
     * @return Model
     */
    public function whereFirst($field, $value): Model
    {
        return $this->builder()->where($field, $value)->firstOrFail();
    }

    /**
     * @param  array $data
     *
     * @return Builder
     */
    public function whereArray(array $data): Builder
    {
        return $this->builder()->where($data);
    }

    /**
     * Update model
     *
     * @param Model $model
     * @param array $data
     * @param bool $withSave
     * @return Model
     */
    public function updateByArray(Model $model, array $data, bool $withSave = true): Model
    {
        $model->fill($data);

        if ($withSave) {
            $model->save();
        }

        return $model;
    }

    /**
     * @param array $data
     *
     * @return void
     */
    public function insert(array $data): void
    {
        $this->builder()->insert($data);
    }

    /**
     * Create Model
     *
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        return $this->builder()->create($data);
    }

    /**
     * Delete Model
     *
     * @param Model $model
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Model $model): ?bool
    {
        return $model->delete();
    }

    /**
     * Save model
     *
     * @param Model|null $model
     * @return bool
     */
    public function save(Model $model = null): bool
    {
        return $model->save();
    }

    /**
     * @param  array $fields
     *
     * @return Model
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function whereArrayFirstOrFail(array $fields): Model
    {
        return $this->builder()->where($fields)->firstOrFail();
    }

    /**
     * @param array $fields
     * @return Builder|Model|object|null
     */
    public function whereArrayFirst(array $fields): ?Model
    {
        return $this->builder()->where($fields)->first();
    }

    /**
     * @param  IteratorAggregate $models
     *
     * @return IteratorAggregate
     */
    public function showHiddenFields(IteratorAggregate $models): IteratorAggregate
    {
        /**
         * @var Model $model
         */
        foreach ($models as $model) {
            $model->showHiddenFields();
        }

        return $models;
    }

    /**
     * @return Model
     */
    public function last(): Model
    {
        return $this->builder()->orderBy('id', 'desc')->first();
    }

    /**
     * @param  bool $withoutNamespace
     *
     * @return string
     *
     * @throws ReflectionException
     */
    public function getModelClass(bool $withoutNamespace = false): string
    {
        $reflectionModel = new ReflectionClass(static::MODEL_REPOSITORY);

        return $withoutNamespace ? $reflectionModel->getShortName() : $reflectionModel->getName();
    }

    /**
     * @return Builder
     */
    public function builder(): Builder
    {
        return call_user_func([static::MODEL_REPOSITORY, 'query']);
    }

    /**
     * @param  int $count
     * @param  callable $callback
     *
     * @return bool
     */
    public function chunk(int $count, callable $callback): bool
    {
        return $this->builder()->chunk($count, $callback);
    }
}
