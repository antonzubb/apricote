<?php

namespace App\Repository;

use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repository
 */
class UserRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = User::class;
}
