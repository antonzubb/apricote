<?php

namespace App\Repositories;

use App\Models\Project;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class ProjectRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = Project::class;
}
