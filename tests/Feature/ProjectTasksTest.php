<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\SetUp\ProjectFactory;
use Tests\TestCase;

class ProjectTasksTest extends TestCase
{
    use WithFaker, RefreshDatabase;


    /**
     * @test
     */
    public function a_user_can_create_task_in_project()
    {
        $this->singIn();

        $project = factory(Project::class)->create(['owner_id' => auth()->id()]);

        $this->post($project->path() . '/tasks' , ['body' => 'test']);

        $this->get($project->path())
            ->assertSee('test');
    }


    /** @test */
    public function a_task_require_a_body()
    {
        $this->singIn();

        $project = factory(Project::class)->create(['owner_id' => auth()->id()]);

        $attributes =  factory(Task::class)->raw(['body' => '']);

        $this->post($project->path() . '/tasks' , $attributes)->assertSessionHasErrors('body');
    }


    /**
     * @test
     */
    public function a_task_can_be_updated()
    {

        $project = app(ProjectFactory::class)
            ->ownedBy($this->singIn())
            ->withTasks(1)
            ->create();

        $this->patch($project->path() . '/tasks/' . $project->tasks()->first()->id , [
            'body' => 'change',
            'completed' => true,
        ]);


        $this->assertDatabaseHas('tasks', [
            'body' => 'change',
            'completed' => true,
        ]);

    }



}
