<?php

namespace Tests\Feature;

use App\Models\Project;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageProjectsTest extends TestCase
{
    use WithFaker, RefreshDatabase;


    /**
     * @test
     */
    public function a_user_can_create_a_project()
    {

        $this->singIn();

        $attributes =  [
            'title' => $this->faker->sentence,
            'description' => $this->faker->sentence,
            'notes' => 'General Notes'
        ];

        $this->get('/projects/create')->assertStatus(200);

        $this->post('/projects', $attributes)->assertRedirect('/projects');

        $project = Project::where($attributes)->first();

        $this->assertDatabaseHas('projects', $attributes);
        $this->get('/projects')->assertSee($attributes['title']);

        $this->get($project->path())
            ->assertSee($attributes['title'])
            ->assertSee($attributes['description'])
            ->assertSee($attributes['notes'])
        ;
    }

    /** @test */
    public function a_user_can_update_a_project()
    {
        $this->singIn();

        $project = factory(Project::class)->create(['owner_id' => auth()->id()]);

        $this->patch($project->path(), [
            'title' => 'required',
            'description' => 'required',
            'notes' => 'changed'
        ])->assertRedirect($project->path());

        $this->assertDatabaseHas('projects', ['notes' => 'changed']);
    }

    /** @test */
    public function a_project_require_a_title()
    {
        $this->singIn();

        $attributes =  factory(Project::class)->raw(['title' => '']);

        $this->post('/projects', $attributes)->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_project_require_a_description()
    {
        $this->singIn();

        $attributes =  factory(Project::class)->raw(['description' => '']);
        $this->post('/projects', $attributes)->assertSessionHasErrors('description');
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function a_user_can_view_their_project()
    {
        $this->singIn();

        $project = factory(Project::class)->create(['owner_id' => auth()->id()]);

        $this->get($project->path())->assertStatus(200)
            ->assertSee($project->title)
            ->assertSee($project->description);

    }



    /**
     * A basic feature test example.
     *
     * @test
     */
    public function an_authenticated_user_cannot_view_their_project()
    {
        $this->singIn();
        $project = factory(Project::class)->create();

        $this->get($project->path())->assertStatus(403);

    }

    public function an_authenticated_user_cannot_update_their_project()
    {
        $this->withExceptionHandling();

        $this->singIn();
        $project = factory(Project::class)->create();

        $this->patch($project->path(), [])->assertStatus(403);

    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function guests_cannot_manage_project()
    {
        $project = factory(Project::class)->create();

        $this->post('/projects', $project->toArray())->assertRedirect('login');
        $this->get('/projects')->assertRedirect('login');
        $this->get('/projects/create')->assertRedirect('login');
        $this->get($project->path())->assertRedirect('login');
    }

    /**
     * A basic feature test example.
     *
     * @test
     */
    public function only_the_owner_of_project_may_add_tasks()
    {
        $this->singIn();

        $text = $this->faker->paragraph;
        $project = factory(Project::class)->create();

        $this->post($project->path() . '/tasks' , ['body' => $text])
           ->assertStatus(403)
        ;

        $this->assertDatabaseMissing('tasks', [ 'body' => $text ]);
    }

}
