<?php


namespace Tests\SetUp;


use App\Models\Project;
use App\Models\Task;
use App\Models\User;

class ProjectFactory
{
    protected $taskCount = 0;
    protected $user;

    public function withTasks(int $count): ProjectFactory
    {
        $this->taskCount = $count;

        return $this;
    }

    public function ownedBy(User $user): ProjectFactory
    {
        $this->user = $user;

        return $this;
    }


    public function create(): Project
    {
        $project = factory(Project::class)->create([
            'owner_id' => $this->user ?? factory(User::class)
        ]);

        factory(Task::class)->create([
            'project_id' => $project->id
        ]);


        return $project;
    }
}
