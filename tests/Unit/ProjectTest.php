<?php

namespace Tests\Unit;

use \App\Models\Project;
use App\Models\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @test
     */
    public function it_has_a_path()
    {
        $project = factory(Project::class)->create();

        $this->assertEquals('/projects/' . $project->id , $project->path());

    }

    /**
     * A basic unit test example.
     *
     * @test
     */
    public function it_belongs_to_an_owner()
    {
        $project = factory(Project::class)->create();

        $this->assertInstanceOf(User::class, $project->owner);

    }

    public function it_create_a_task()
    {
        $project = factory(User::class)->create();

        $task = $project->addTask('Test Task');

        $this->assertCount(1 , $project->tasks);
        $this->assertTrue($project->tasks()->contains($task));
    }
}
