@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Forum Dashboard</div>

                    <div class="card-body">
                        @foreach($projects as $project)
                            <article>
                                <a href="{{$project->path()}}">
                                    <h4>{{ $project->title }}</h4>
                                </a>
                                <div class="body">
                                    {{ $project->description }}
                                </div>
                            </article>
                            <hr>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
