@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Forum Dashboard</div>

                    <div class="card-body">
                            <article>
                                <h4>{{ $project->title }}</h4>
                                <div class="body">
                                    {{ $project->description }}
                                </div>


                            </article>
                            <hr>

                    </div>
                    <div class="card-body">
                        @foreach($project->tasks as $task)
                            <article>
                                <div class="body">
                                    {{ $task->body }}
                                </div>
                            </article>
                        <hr>
                        @endforeach

                    </div>

                    <div class="card-body">
                        {{$project->notes}}
                    </div>

                    <form action="POST" method="{{$project->path()}}">
                        @csrf
                        @method('PATCH')
                    </form>

                </div>
            </div>
        </div>
    </div>
@endsection
