@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a Project</div>

                    <div class="card-body">
                        <form method="POST" action="/projects">
                            @csrf
                            <input name="title" type="text">
                            <input name="description" type="text">
                            <button type="submit">ok</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
