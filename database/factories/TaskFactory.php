<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Task::class, function (Faker $faker) {
    return [
        'body' => $faker->paragraph,
        'project_id'  => function () {
            return factory(\App\Models\Project::class)->create()->id;
        }
    ];
});
