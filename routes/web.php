<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/projects', 'ProjectsController@index');
    Route::get('/projects/create', 'ProjectsController@create');
    Route::get('/projects/{project}', 'ProjectsController@show');
    Route::patch('/projects/{project}', 'ProjectsController@update');
    Route::post('/projects/{project}/tasks', 'ProjectsTaskController@store');
    Route::patch('/projects/{project}/tasks/{task}', 'ProjectsTaskController@update');
    Route::post('/projects', 'ProjectsController@store');
});


